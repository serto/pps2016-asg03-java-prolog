package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[4][4];
    private final JFrame frame = new JFrame("TTT");
    private Player turn = Player.PlayerX;
    private int nGame = 0;
    private String[] winner = new String[3];

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt=ttt;
        initPane();
    }
    
    private void humanMove(int i, int j){
        if (ttt.move(turn,i,j)){
            board[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            changeTurn();
        }
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()){
            winner[nGame] = victory.get().name();
            JOptionPane.showMessageDialog(null, victory.get().name() + " win the manche!", "Win", JOptionPane.INFORMATION_MESSAGE);
            restart(victory);
        }
        if (ttt.checkCompleted()){
            winner[nGame] = "Even";
            JOptionPane.showMessageDialog(null, "Even!", "Even", JOptionPane.INFORMATION_MESSAGE);
            restart(victory);
        }
    }

    private void restart(Optional<Player> victory){
        nGame++;
        if(nGame == 3){
            if(winner[0].equals("PlayerX") && winner[1].equals("PlayerX") ||
                    winner[0].equals("PlayerX") && winner[2].equals("PlayerX") ||
                    winner[1].equals("PlayerX") && winner[2].equals("PlayerX")){
                JOptionPane.showMessageDialog(null, "Player X WON!!", "Win", JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }else if(winner[0].equals("PlayerO") && winner[1].equals("PlayerO") ||
                    winner[0].equals("PlayerO") && winner[2].equals("PlayerO") ||
                    winner[1].equals("PlayerO") && winner[2].equals("PlayerO")){
                JOptionPane.showMessageDialog(null, "Player O WON!!", "Win", JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            } else{
                JOptionPane.showMessageDialog(null, "Even!", "Even!", JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }
        }
        for (int ic = 0; ic < 4; ic++) {
            for (int jc = 0; jc < 4; jc++) {
                board[ic][jc].setText("");
            }
        }
        ttt.createBoard();
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b = new JPanel(new GridLayout(4,4));
        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 4; j++){
                final int i2 = i;
                final int j2 = j;
                board[i][j] = new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> {humanMove(i2,j2); });
            }
        }
        JPanel s = new JPanel(new FlowLayout());
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(200,200);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        try {
            new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
